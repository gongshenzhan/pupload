# layui扩展组件之pupload上传插件

#### 项目介绍
采用LayUI 第三方插件开发规范（[https://fly.layui.com/jie/29160/](https://fly.layui.com/jie/29160/)）集成的plupload组件，后期将扩展成ui直接拿来即用的组件


#### 使用说明

页面元素
<!-- 你的HTML代码 -->


```
<button id="plupload" class="layui-btn layui-btn-normal"><i class="layui-icon layui-icon-upload"></i>上传测试</button>
<div id="hahaha" style="width:1000px;height:300px;border:1px solid #333">
    拖拽上传
</div>

```

js部分


```

layui.config({
        base: '../dist/mods/'
    }).use('mods', function (mods) {
        // pupload是扩展插件。
        mods(['layer', 'form', 'pupload'], function (layer, form, pupload) {
            //var $ = layui.$;
            pupload.loader({
                url: '/dream/file/upload?path=upload/file/',
                browse_button: "plupload" 
                , drop_element: "hahaha"
            }, function (uploader) {
                console.log(uploader)
                uploader.init();
            });//
        });
    });

```


更多API使用方式可参考plupload官方文档（ https://www.plupload.com/ ）

后期将继续升级维护该组件（一系列场景的ui组件），为了防止重名，组件改名为pupload。

#### 参与贡献
龚申展 - 上海梦创双杨